import "./App.css";
// import TodoForm from "./components/todoForm";
import { RegisterForm } from "./register/RegisterForm";

function App() {
  return (
    <div className="App">
      <RegisterForm />
    </div>
  );
}

export default App;
