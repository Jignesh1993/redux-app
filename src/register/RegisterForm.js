import React, { useState } from "react";
import { register } from "./actions/registerAction";

export const RegisterForm = () => {
  const [name, setname] = useState();
  const [email, setemail] = useState();
  const [mobile, setmobile] = useState();
  const [age, setage] = useState(0);
  const [city, setcity] = useState();
  const [password, setpassword] = useState();

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      name: name,
      email: email,
      mobile: mobile,
      age: parseInt(age),
      city: city,
      password: password,
    };
    register(data);
  };
  return (
    <div className="container">
      <label style={{ fontSize: 25, fontWeight: "bold" }}>Registration</label>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          id="name"
          placeholder="Name"
          onChange={(e) => setname(e.target.value)}
        />
        <input
          type="text"
          id="email"
          placeholder="Email"
          onChange={(e) => setemail(e.target.value)}
        />
        <input
          type="text"
          id="mobile"
          placeholder="Mobile"
          onChange={(e) => setmobile(e.target.value)}
        />
        <input
          type="text"
          id="age"
          placeholder="Age"
          onChange={(e) => setage(e.target.value)}
        />
        <input
          type="text"
          id="city"
          placeholder="City"
          onChange={(e) => setcity(e.target.value)}
        />
        <input
          type="password"
          id="password"
          placeholder="Password"
          onChange={(e) => setpassword(e.target.value)}
        />
        <button className="btn green" onClick={handleSubmit}>
          Register
        </button>
      </form>
    </div>
  );
};
