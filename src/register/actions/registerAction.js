import axios from "axios";
import Toast from "light-toast";

export const register = async (data) => {
  await axios
    .post(`${process.env.REACT_APP_API_URL}register`, data)
    .then(function (response) {
      Toast.success("Registered..!", 2000);
    })
    .catch(function (error) {
      Toast.fail("Failed..!", 2000);
    });
  Toast.hide();
};
